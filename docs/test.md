# Using CLI for gitlab
Gitlab CLI has a rich set of CLI that makes it easier to work locally from your favourite laptop/machine and sync your changes to the main "remote" repository. 
## Steps for setting up the local repository
 - Generate a SSH key to connect in your local pc
 - Add your SSH key to your remote repository
 - Clone a local repository of your project
 - Check the status, track, commit and push

